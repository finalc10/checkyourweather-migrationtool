﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IMT_CheckYourWeather
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public delegate void LoggingDelegate(string Message);
    public partial class MainWindow : Window
    {
        private List<MeasurementSet> measurements;
        public MainWindow()
        {
            InitializeComponent();
            measurements = new List<MeasurementSet>();
        }

        private void OnImportData(object sender, RoutedEventArgs e)
        {
            // Create a new instance of the ImportDialog
            ImportDialog dia = new ImportDialog();
            // Open the ImportDialog
            dia.ShowDialog();
            // Check if the dialog has some templates to import
            if (dia.ImportFiles)
            {
                List<string> filepaths = dia.FilePaths.ToList();
                List<MeasurementFile> files = new List<MeasurementFile>();
                lbMessages.Items.Add("Deleting priviously imported files");
                Logger.GetLogger(Logger.DefaultClientLogger).LogMessage("Deleting priviously imported files", LogLevel.INFO);
                AdjustView();
                foreach (var s in filepaths)
                {
                    //gettings files
                    files.Add(ImportFile(s));
                }
                if (files.Count > 0)
                {
                    //Logging Actions
                    tbFileName.Text = "";
                    tbStatus.Text = "Preparing";
                    lbMessages.Items.Add("Prepare Parsing Process ...");
                    AdjustView();
                    //parsing Process
                    ParseFiles(files);
                }
            }
        }

        private MeasurementFile ImportFile(string s)
        {
            try
            {
                MeasurementFile m = new MeasurementFile();
                m.FileName = s;
                StreamReader sr = new StreamReader(s, Encoding.UTF7);
                m.Content = sr.ReadToEnd();
                sr.Close();
                //Logging Actions
                tbStatus.Text = " imported File: ";
                tbFileName.Text = s;
                lbMessages.Items.Add("File " + s + " successfully imported");
                AdjustView();
                Logger.GetLogger(Logger.DefaultClientLogger).LogMessage("File " + s + " successfully imported", LogLevel.INFO);
                return m;
            }
            catch (Exception e)
            {
                
                Logger.GetLogger(Logger.DefaultClientLogger).LogMessage("Error loading File: " + s + "Exception: " + e.Message, LogLevel.ERROR);
                tbStatus.Text = "Error: ";
                tbFileName.Text = s;
                lbMessages.Items.Add("Error loading File, for details view the log file: " + s);
                AdjustView();
            }
            return null;
        }

        private void ParseFiles(List<MeasurementFile> files)
        {
            MigrationParser.loggingEvent += GUILogging;
            //measurements = data actually inside the program
            measurements = MigrationParser.ParseInputFiles(files);
            tbStatus.Text = "Ready";
            this.SizeToContent = SizeToContent.Width;
        }

        public void GUILogging(string message)
        {
            lbMessages.Items.Add(message);
            AdjustView();
        }

        public void AdjustView()
        {
            lbMessages.SelectedIndex = lbMessages.Items.Count - 1;
            lbMessages.ScrollIntoView(lbMessages.SelectedItem);
        }

        private void OnOpenLogFileLocation(object sender, RoutedEventArgs e)
        {
            string tempfolderpath = Logger.GetLogger(Logger.DefaultClientLogger).LogDirectory;
            string windir = Environment.GetEnvironmentVariable("WINDIR");
            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = windir + @"\explorer.exe";
            prc.StartInfo.Arguments = tempfolderpath;
            prc.Start();
        }
    }
}
