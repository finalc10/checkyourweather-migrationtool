﻿using System;
using System.Collections.Generic;
using System.IO;

namespace IMT_CheckYourWeather
{
      /// <summary>
    /// The different log-levels used by the logger
    /// </summary>
    public enum LogLevel
    {
        ERROR=0, WARN=1, DEBUG=2, INFO=3
    }
        /// <summary>
    /// Logger class
    /// Used to log all the different informations
    /// </summary>
    public class Logger
    {
        // Writes to the log file
        private StreamWriter _logWriter;
        // Name of the logger
        private string _name;
        // Filename of the log file
        private string _logFileName;
        // Full path to the log file
        private string _logPath;
        // Defines how much information is logged
        private LogLevel _logLevel;

        public string LogDirectory
        {
            get { return _logPath.Replace(_logFileName, ""); }
        }

        public LogLevel LoggingLevel
        {
            get { return _logLevel; }
            set { _logLevel = value; }
        }

        /// <summary>
        /// Logger constructor
        /// </summary>
        /// <param name="name">Name of the logger</param>
        public Logger(string name)
        {
            string date = DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "-" + DateTime.Now.Minute;
            this._name = name;
            this._logFileName = name + "_" + date + ".log";
            this._logPath = Path.GetTempPath() + _logFileName;
            // Default log-level: INFO
            _logLevel = LogLevel.INFO;
            // Create StreamWriter for the log-file and set append to true
            _logWriter = new StreamWriter(_logPath, true);
        }

        /// <summary>
        /// Writes the given message to the log-file
        /// </summary>
        /// <param name="message">Message which should be written to the log-file</param>
        /// <param name="level">LogLevel of the message</param>
        public void LogMessage(string message, LogLevel level)
        {
            // If the level of the message is higher than
            // the log-level of the logger, then don't log the message
            if (level > _logLevel)
            {
                return;
            }
            string line = String.Format("{0}{1}", GetLinePrefix(level), message);
            _logWriter.WriteLine(line);
            _logWriter.Flush();
        }

        /// <summary>
        /// Returns the prefix for a log line with a specific log-level
        /// Also appends the current date and time
        /// </summary>
        /// <param name="level">Log-level of the line</param>
        /// <returns>Returns the prefix for a log line</returns>
        private string GetLinePrefix(LogLevel level)
        {
            return String.Format("[{0,-5} >> {1} - {2} ]: ", level, DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString());
        }


        // Holds all the currently existing loggers
        private static Dictionary<string, Logger> _currentLoggers = new Dictionary<string,Logger>();

        // Name of the default logger
        public static readonly string DefaultClientLogger = "migrationlogger";

        // Logger.GetLogger(Logger.imtlogger).LogMessage("message", LogLevel.WARN);
        /// <summary>
        /// Returns the logger based on the given name
        /// If the logger does not exist, it will be created
        /// </summary>
        /// <param name="name">The name of the logger</param>
        /// <returns>Returns the logger with the given name</returns>
        public static Logger GetLogger(string name)
        {
            Logger l;
            // Try if the logger already exists
            _currentLoggers.TryGetValue(name, out l);
            // If it does not exist yet...
            if (l == null)
            {
                // ... create a new one and add it to the list of loggers
                l = new Logger(name);
                _currentLoggers.Add(name, l);
            }
            return l;
        }
    }
}
