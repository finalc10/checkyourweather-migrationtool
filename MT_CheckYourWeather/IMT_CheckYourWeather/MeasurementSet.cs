﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMT_CheckYourWeather
{
    public class MeasurementSet
    {
        private List<Measurement> measurements = new List<Measurement>();

        public List<Measurement> Measurements
        {
            get { return measurements; }
            set { measurements = value; }
        } 
        //Which parameter is measured? e.g. temperature
        public string parameterOfMeasurement { get; set; }
        //Which unit is used? e.g. °C
        public string measurementUnit { get; set; }
        public DateTime timeStamp { get; set; }

        //Other Attributes of Temperature missing (??)
    }
}
