﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMT_CheckYourWeather
{
    public class MeasurementFile
    {
        //Including path
        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        private string content;

        public string Content
        {
            get { return content; }
            set { content = value; }
        }
        
        

    }
}
