﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MessageBox = System.Windows.Forms.MessageBox;

namespace IMT_CheckYourWeather
{
    public static class MigrationParser
    {
        public static event LoggingDelegate loggingEvent;
        /// <summary>
        /// Parser to Parse the input files into MeasurementSets
        /// </summary>
        /// <param name="filelist">List of all the input files (name, content)</param>
        /// <returns></returns>
        public static List<MeasurementSet> ParseInputFiles(List<MeasurementFile> filelist)
        {
            List<MeasurementSet> measurementSets = new List<MeasurementSet>();
            MeasurementSet m;
            foreach (var measurementFile in filelist)
            {
                m = new MeasurementSet();
                string name = measurementFile.FileName.Split('\\').Last();
                try
                {
                    m.parameterOfMeasurement =
                        name.Split(new string[] { "Stationen" }, StringSplitOptions.RemoveEmptyEntries)[0];
                    string date = name.Split(new string[] { "Stationen" }, StringSplitOptions.RemoveEmptyEntries)[1];
                    m.timeStamp = new DateTime(
                        int.Parse(date.Substring(0, 4)),
                        int.Parse(date.Substring(4, 2)),
                        int.Parse(date.Substring(6, 2)),
                        int.Parse(date.Substring(8, 2)),
                        int.Parse(date.Substring(10, 2)),
                        0, 0
                        //int.Parse(date.Substring(13, 4))/60,
                        //int.Parse(date.Substring(13, 4))%60
                        );
                    string[] lines = measurementFile.Content.Split(new string[] { "\r\n", "\n" },
                        StringSplitOptions.RemoveEmptyEntries);
                    m.measurementUnit = lines[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[2];
                    loggingEvent("MeasurementSet for " + m.parameterOfMeasurement + " on " + m.timeStamp.ToString() +
                                 "added. Unit: " + m.measurementUnit);
                    Logger.GetLogger(Logger.DefaultClientLogger).LogMessage("MeasurementSet for " +
                        m.parameterOfMeasurement + " on " + m.timeStamp.ToString() + "added. Unit: " + m.measurementUnit, LogLevel.INFO);
                    for (int i = 0; i < lines.Length; i++)
                    {
                        if (i != 0)
                        {
                            Measurement measurement = parseLine(lines[i]);
                            m.Measurements.Add(measurement);
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.GetLogger(Logger.DefaultClientLogger).LogMessage("File format invalid at: " +
                                                                            measurementFile.FileName + " Exception: " +
                                                                            e.Message, LogLevel.ERROR);
                    var dialogResult = MessageBox.Show(@"File format invalid at: " +
                                                       measurementFile.FileName + @" Exception: " + e.Message);
                }
                measurementSets.Add(m);
            }
            return measurementSets;
        }

        private static Measurement parseLine(string p)
        {
            string[] attributes = p.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string name = "";
            for (int i = 4; i < attributes.Length - 1; i++)
            {
                name += attributes[i] + " ";
            }
            name += attributes[attributes.Length - 1];
            Measurement m = new Measurement(
                int.Parse(attributes[0]),
                int.Parse(attributes[1]),
                int.Parse(attributes[3]),
                name,
                double.Parse(attributes[2])
                );
            loggingEvent("Measurement added: X:" + m.Koordinaten.X + " Y:" +
                         m.Koordinaten.Y + " sea level:" + m.Seehoehe + " name of station:" + m.StationName + " value:" +
                         m.Wert);
            Logger.GetLogger(Logger.DefaultClientLogger).LogMessage(
                "Measurement added: X:" + m.Koordinaten.X + " Y:" +
                         m.Koordinaten.Y + " sea level:" + m.Seehoehe + " name of station:" + m.StationName + " value:" +
                         m.Wert, LogLevel.INFO);
            return m;
        }
    }
}
