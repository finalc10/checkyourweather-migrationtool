﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace IMT_CheckYourWeather
{
    /// <summary>
    /// Describes a single Measurement inside a List
    /// </summary>
    public class Measurement
    {
        #region Fields
        private Point point;
        public Point Koordinaten
        {
            get { return point; }
            set { point = value; }
        }

        private double wert;

        public double Wert
        {
            get { return wert; }
            set { this.wert = value; }
        }

        private int seehoehe;

        public int Seehoehe
        {
            get { return seehoehe; }
            set { seehoehe = value; }
        }

        private string stationName;

        public string StationName
        {
            get { return stationName; }
            set { stationName = value; }
        } 
	#endregion

        public Measurement(int x, int y, int hoehe, string name, double wert)
        {
            point = new Point(x,y);
            this.seehoehe = hoehe;
            this.stationName = name;
            this.wert = wert;
        }
    }
}
