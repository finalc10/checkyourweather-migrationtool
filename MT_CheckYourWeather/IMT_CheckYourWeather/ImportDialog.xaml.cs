﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IMT_CheckYourWeather
{
    /// <summary>
    /// Interaction logic for ImportDialog.xaml
    /// </summary>
    public partial class ImportDialog : Window, INotifyPropertyChanged
    {
        private ObservableCollection<string> filepaths = new ObservableCollection<string>();

        public ObservableCollection<string> FilePaths
        {
            get { return filepaths; }
        }

        private bool importFiles = new bool();

        public bool ImportFiles
        {
            get { return importFiles; }
        }
        public ImportDialog()
        {
            InitializeComponent();
            this.DataContext = this;
            importFiles = false;
        }

        private void OnAddFileToImport(object sender, RoutedEventArgs e)
        {
            // Initialise new OpenFileDialog
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            // Allow multiselect
            ofd.Multiselect = true;
            // Set filter of OpenFileDialog
            ofd.Filter = "Measurement File (.txt) | *.txt";
            // Check if the Ok button is pressed on the OpenFileDialog
            if (ofd.ShowDialog() == true)
            {
                ofd.FileNames.ToList().ForEach(x =>
                {
                    if (filepaths.Contains(x))
                    {
                        MessageBox.Show("File already in insertion List: "+ x);
                        Logger.GetLogger(Logger.DefaultClientLogger).LogMessage(("File already in insertion List: "+ x + " no multiple insertion possible"), LogLevel.WARN);
                    }
                    else
                    {
                        filepaths.Add(x);
                    }

                });
                OnPropertyChanged("FilePaths");
            }
            this.SizeToContent = SizeToContent.WidthAndHeight;
        }

        private void OnDeleteFileToImport(object sender, RoutedEventArgs e)
        {
            while (lvFiles.SelectedItems.Count > 0)
            {
                filepaths.Remove((string)lvFiles.SelectedItems[0]);
            }
        }

        private void OnImport(object sender, RoutedEventArgs e)
        {
            importFiles = true;
            this.Close();
        }

        private void OnCancelImport(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OnListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvFiles.SelectedItems.Count > 0)
            {
                btRemove.IsEnabled = true;
            }
            else
            {
                btRemove.IsEnabled = false;
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string source)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(source));
            }
        }
    }
}
